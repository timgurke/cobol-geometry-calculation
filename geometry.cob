       IDENTIFICATION DIVISION.
       PROGRAM-ID. Flächeninhalt.
       ENVIRONMENT DIVISION.

       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01 VARIABLEN.
           05 KONSTANTEN.
               10 PI PIC 9V99 VALUE 3.14.
           05 HILFSVARAIBLEN.
               10 ERGEBNIS PIC 9(4).
           05 EINGABE-VARIABLEN.
               10 EINGABE-SCHLUESSEL PIC X(8).
               10 EINGABE-WERT1 PIC 9(2).
               10 EINGABE-WERT2 PIC 9(2).
               10 EINGABE-WERT3 PIC 9(2).
           05 AUSGABE-VARIABLEN.
               10 FEHLER-EINGABE PIC X(30) VALUE "Der Eingabewert ist ungueltig!".
               10 FEHLER-ZAHLENBEREICH PIC X(41) VALUE "Nur Werte zwischen 1 und 10 sind erlaubt!".
               10 ERGEBNIS-AUSGABE.
                   15 FILLER PIC X(22) VALUE "Die Flaeche betraegt: ".
                   15 ERGEBNIS-LESBAR PIC ZZZ9.
       01 SCHALTER.
           05 SCHLUESSEL PIC A(8).
               88 ZYLINDER VALUE "zylinder".
               88 KEGEL VALUE "kegel".
               88 QUADRAT VALUE "quadrat".
               88 DREIECK VALUE "dreieck".
               88 KUGEL VALUE "kugel".
               88 QUADER VALUE "quader".

       PROCEDURE DIVISION.

       PROGRAMM-ANFANG SECTION.
           PERFORM EINGABE.
           PERFORM VALIDIERUNG.
           PERFORM BERECHNUNG.
           PERFORM AUSGABE.
           PERFORM PROGRAMM-ENDE.

       EINGABE SECTION.
           ACCEPT SCHLUESSEL.
           EVALUATE TRUE
               WHEN QUADRAT
               WHEN KUGEL
                   ACCEPT EINGABE-WERT1
                   MOVE 1 TO EINGABE-WERT2
                   MOVE 1 TO EINGABE-WERT3
               WHEN ZYLINDER
               WHEN KEGEL
               WHEN DREIECK
                   ACCEPT EINGABE-WERT1
                   ACCEPT EINGABE-WERT2
                   MOVE 1 TO EINGABE-WERT3
               WHEN QUADER
                   ACCEPT EINGABE-WERT1
                   ACCEPT EINGABE-WERT2
                   ACCEPT EINGABE-WERT3
               WHEN OTHER
                   DISPLAY FEHLER-EINGABE
                   PERFORM PROGRAMM-ENDE
           END-EVALUATE.

       VALIDIERUNG SECTION.
           IF EINGABE-WERT1 < 1
               OR EINGABE-WERT1 > 10
               OR EINGABE-WERT2 < 1
               OR EINGABE-WERT2 > 10
               OR EINGABE-WERT3 < 1
               OR EINGABE-WERT3 > 10 THEN

               DISPLAY FEHLER-ZAHLENBEREICH
               PERFORM PROGRAMM-ENDE
           END-IF.

       BERECHNUNG SECTION.
           EVALUATE TRUE
               WHEN ZYLINDER
                   PERFORM BERECHNUNG-ZYLINDER
               WHEN KEGEL
                   PERFORM BERECHNUNG-KEGEL
               WHEN QUADRAT
                   PERFORM BERECHNUNG-QUADRAT
               WHEN DREIECK
                   PERFORM BERECHNUNG-DREIECK
               WHEN KUGEL
                   PERFORM BERECHNUNG-KUGEL
               WHEN QUADER
                   PERFORM BERECHNUNG-QUADER
           END-EVALUATE.

       BERECHNUNG-ZYLINDER SECTION.
           COMPUTE ERGEBNIS ROUNDED = 2 * PI * EINGABE-WERT1 * (EINGABE-WERT1 + EINGABE-WERT2).

       BERECHNUNG-KEGEL SECTION.
           COMPUTE ERGEBNIS ROUNDED = PI * EINGABE-WERT1 ** 2 + PI * EINGABE-WERT1 * (EINGABE-WERT1 ** 2 + EINGABE-WERT2 ** 2) ** 0.5.

       BERECHNUNG-QUADRAT SECTION.
           COMPUTE ERGEBNIS ROUNDED = EINGABE-WERT1 ** 2.

       BERECHNUNG-DREIECK SECTION.
           COMPUTE ERGEBNIS ROUNDED = 0.5 * EINGABE-WERT1 * EINGABE-WERT2.

       BERECHNUNG-KUGEL SECTION.
           COMPUTE ERGEBNIS ROUNDED = 4 * PI * EINGABE-WERT1 ** 2.

       BERECHNUNG-QUADER SECTION.
           COMPUTE ERGEBNIS ROUNDED = 2 * (EINGABE-WERT1 * EINGABE-WERT2 + EINGABE-WERT2 * EINGABE-WERT3 + EINGABE-WERT1 * EINGABE-WERT3).

       AUSGABE SECTION.
           MOVE ERGEBNIS TO ERGEBNIS-LESBAR.
           DISPLAY ERGEBNIS-AUSGABE.

       PROGRAMM-ENDE SECTION.
           STOP RUN.
